package com.jlopez.testjetty

import com.jlopez.testjetty.utils.Utils
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class EmailValidatorTest {

    @Test
    fun emailValidator_CorrectEmailSubDomain_ReturnsTrue() {
        assertTrue(Utils.isValidEmail("irvin@jetty.mx"))
    }

    @Test
    fun emailValidator_InvalidEmailNoTld_ReturnsFalse() {
        //assertFalse(false)
        assertFalse(Utils.isValidEmail("irvin@jetty"))
    }
}