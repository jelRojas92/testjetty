package com.jlopez.testjetty

import android.app.Application
import android.util.Log
import androidx.lifecycle.Observer
import com.jlopez.testjetty.common.Constant
import com.jlopez.testjetty.network.APIService
import com.jlopez.testjetty.network.RetrofitHelper
import com.jlopez.testjetty.network.request.Driver
import com.jlopez.testjetty.network.request.LoginRequest
import com.jlopez.testjetty.network.response.LoginResponse
import com.jlopez.testjetty.viewmodel.LoginViewModel
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@RunWith(JUnit4::class)
class EndPointTest {

    @Mock
    lateinit var mainViewModel: LoginViewModel

    lateinit var app: Application
    lateinit var apiService: APIService

    @Before
    fun setUp() {
        app = Application()
        MockitoAnnotations.initMocks(this)
        this.mainViewModel = LoginViewModel(app)
        apiService = RetrofitHelper.getAPIService()

    }
    @Test
    fun loginUser_positiveResponse() {
        apiService?.loginUser(Constant.Headers.CONTENT_TYPE_VALUE, LoginRequest(Driver("irvin@jetty.mx", "123456")))
            ?.enqueue(object :
                Callback<LoginResponse> {

                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    assertEquals(200, response.code())
                }
                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    assertNotNull(t)
                }
            })

    }
}