package com.jlopez.testjetty.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jlopez.testjetty.common.Constant
import com.jlopez.testjetty.network.APIService
import com.jlopez.testjetty.network.RetrofitHelper
import com.jlopez.testjetty.network.response.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TripsRepository {
    private var apiService: APIService? = RetrofitHelper.getAPIService()
    val data = MutableLiveData<TripsResponseOnComplete>()

    fun getTrips(auth: String): LiveData<TripsResponseOnComplete> {
        val result = TripsResponseOnComplete()
        apiService?.getTrips(Constant.Headers.CONTENT_TYPE_VALUE, auth)?.enqueue(object :
            Callback<List<DataGeneralResponse>> {
            override fun onResponse(call: Call<List<DataGeneralResponse>>, response: Response<List<DataGeneralResponse>>) {
                if(response.code() == 200) {
                    result.objeto = response.body()
                }
                result.statusCode = response.code()
                data.postValue(result)
            }

            override fun onFailure(call: Call<List<DataGeneralResponse>>, t: Throwable) {
                result.throwable = t
                data.postValue(result)
            }
        })
        return data
    }
}