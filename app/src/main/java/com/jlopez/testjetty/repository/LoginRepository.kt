package com.jlopez.testjetty.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.jlopez.testjetty.network.APIService
import androidx.lifecycle.LiveData
import com.jlopez.testjetty.network.response.LoginResponse
import com.jlopez.testjetty.network.RetrofitHelper
import com.jlopez.testjetty.common.Constant
import com.jlopez.testjetty.network.request.Driver
import com.jlopez.testjetty.network.request.LoginRequest
import com.jlopez.testjetty.network.response.LoginResponseOnComplete
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginRepository {
    private var apiService: APIService? = RetrofitHelper.getAPIService()
    val data = MutableLiveData<LoginResponseOnComplete>()

    fun loginUser(email: String, password: String): LiveData<LoginResponseOnComplete> {
        val result = LoginResponseOnComplete()
        apiService?.loginUser(Constant.Headers.CONTENT_TYPE_VALUE, LoginRequest(Driver( email, password)))?.enqueue(object :
            Callback<LoginResponse> {

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if(response.code() == 200) {
                    result.objeto = response.body()
                }
                result.statusCode = response.code()
                data.postValue(result)
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                result.throwable = t
                data.postValue(result)
            }
        })
        return data
    }
}