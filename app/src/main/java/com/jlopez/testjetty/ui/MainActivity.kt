package com.jlopez.testjetty.ui

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.jlopez.testjetty.R
import com.jlopez.testjetty.utils.PreferencesHelper
import com.jlopez.testjetty.utils.Utils
import com.jlopez.testjetty.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    private var loginViewModel: LoginViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        object : CountDownTimer(2000, 1000) {
            override fun onFinish() {
                if(PreferencesHelper.getAutn(this@MainActivity)!!.isEmpty()) {
                    loadingProgressBar.visibility = View.GONE
                    rootView.setBackgroundColor(ContextCompat.getColor(this@MainActivity,
                        R.color.colorSplashText
                    ))
                    bookIconImageView.setImageResource(R.drawable.logo)
                    startAnimation()
                } else {
                    loadNextActivity()
                    finish()
                }
            }

            override fun onTick(p0: Long) {}
        }.start()
        loginViewModel!!.loginRepository.data.observe(this,  Observer {
            when {
                it.statusCode == 200 -> {
                    PreferencesHelper.setAuth(this@MainActivity, it.objeto!!.authToken)
                    PreferencesHelper.setEmail(this@MainActivity, it.objeto!!.email)
                    loadNextActivity()
                    finish()
                }
                it.statusCode == 401 -> Toast.makeText(this@MainActivity, getString(R.string.login_error_incorrect), Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(this@MainActivity, getString(R.string.login_error_server), Toast.LENGTH_SHORT).show()
            }
        })
        loginButton.setOnClickListener{
            var error = validateFields()
            if(error.isNotEmpty()) {
                generateMessageDialog(
                    getString(R.string.error),
                    error,
                    getString(R.string.ok),
                    false,
                    null, null
                )
                return@setOnClickListener
            }

            displayLoadingDialog()
            loginViewModel!!.loginRepository.data.observe(this,  Observer {

            })
            loginViewModel!!.loginUser(emailEditText.text.toString().trim(),passwordEditText.text.toString().trim())
                .observe(this, Observer {
                hideLoadingDialog()
            })
        }
    }

    private fun loadNextActivity() {
        val intent = Intent(this@MainActivity, ListTripsActivity::class.java)
        startActivity(intent)
    }
    private fun validateFields(): String {
        var error = ""
        if(emailEditText.text!!.isEmpty()) {
            error += "- " + getString(R.string.email_empty) + "\n"
        } else if (!Utils.isValidEmail(emailEditText.text!!.toString())) {
            error += "- " + getString(R.string.email_invalid) + "\n"
        }
        if(passwordEditText.text!!.isEmpty()) {
            error += "- " + getString(R.string.password_empty) + "\n"
        }
        return error;
    }
    private fun startAnimation() {
        bookIconImageView.animate().apply {
            x(bookIconImageView.x)
            y(100f)
            duration = 500
        }.setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) {

            }

            override fun onAnimationEnd(p0: Animator?) {
                afterAnimationView.visibility = View.VISIBLE
            }

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationStart(p0: Animator?) {

            }
        })
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            hideSystemUI()
        }
    }
}
