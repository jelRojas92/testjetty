package com.jlopez.testjetty.ui

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.jlopez.testjetty.R
import com.jlopez.testjetty.ui.adapter.TripsAdapter
import com.jlopez.testjetty.common.Constant
import com.jlopez.testjetty.utils.PreferencesHelper
import com.jlopez.testjetty.viewmodel.TripsViewModel
import kotlinx.android.synthetic.main.activity_list_trips.*

class ListTripsActivity : BaseActivity() {
    private var tripsAdapter: TripsAdapter? = null
    private var tripsViewModel: TripsViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_trips)
        title = getString(R.string.list_trip_name)

        tripsViewModel = ViewModelProviders.of(this).get(TripsViewModel::class.java)
        displayLoadingDialog()
        tripsViewModel!!.loginUser(String.format(Constant.Headers.AUTHORIZATION_VALUE,
            PreferencesHelper.getAutn(this@ListTripsActivity) , PreferencesHelper.getEmail(this@ListTripsActivity)))
            .observe(this, Observer {
                hideLoadingDialog()
                when {
                    it.statusCode == 200 -> {
                        tripsAdapter = TripsAdapter(this@ListTripsActivity, it.objeto!!)
                        rvTrips.layoutManager = LinearLayoutManager(this@ListTripsActivity)
                        rvTrips.adapter = tripsAdapter
                    }
                    it.statusCode == 401 -> {
                        Toast.makeText(this@ListTripsActivity, getString(R.string.session_old), Toast.LENGTH_SHORT).show()
                        PreferencesHelper.setAuth(this@ListTripsActivity, "")
                        PreferencesHelper.setEmail(this@ListTripsActivity, "")
                        finish()
                    }
                    else -> Toast.makeText(this@ListTripsActivity, getString(R.string.login_error_server), Toast.LENGTH_SHORT).show()
                }

            })
    }
}
