package com.jlopez.testjetty.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jlopez.testjetty.R
import com.jlopez.testjetty.network.response.TripStopsResponse
import kotlinx.android.synthetic.main.item_stops.view.*

class TripStopAdapter(
    val context: Context,
    private val items: List<TripStopsResponse>) : RecyclerView.Adapter<TripStopAdapter.StopViewHolder>() {

    private var lastPosition = 0
    private var dateShowing = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StopViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_stops, parent, false)
        return StopViewHolder(view)
    }

    override fun onBindViewHolder(holder: StopViewHolder, position: Int) {


        holder.tvCountStop?.text =  "" + (position + 1)
        holder.tvNameStop?.text = items[position].name
        holder.tvAddresStop?.text = items[position].address
        if(isLast(position)) holder.vLine?.visibility = View.INVISIBLE
    }

    override fun getItemCount() = items.size

    fun isLast(position: Int) : Boolean {
        return (items.size - 1) == position
    }

    class StopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvCountStop: TextView? = itemView.countStop
        val tvNameStop: TextView? = itemView.nameStop
        val tvAddresStop: TextView? = itemView.addressStop
        val vLine: View? = itemView.lineSeparator
    }
}