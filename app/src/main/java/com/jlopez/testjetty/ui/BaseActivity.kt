package com.jlopez.testjetty.ui

import android.app.AlertDialog
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.jlopez.testjetty.R

abstract class BaseActivity : AppCompatActivity() {

    var loadingDialog: AlertDialog? = null

    protected fun generateLoadingDialog() {
        val loadingDialogBuilder = AlertDialog.Builder(this@BaseActivity)

        val mView = layoutInflater.inflate(R.layout.loading_dialog, null)
        loadingDialogBuilder.setView(mView)

        loadingDialog = loadingDialogBuilder.create()
        loadingDialog!!.setCancelable(false)
        loadingDialog!!.setCanceledOnTouchOutside(false)
    }

    protected fun displayLoadingDialog() {
        if (this.loadingDialog == null)
            generateLoadingDialog()

        loadingDialog!!.show()
    }

    protected fun hideLoadingDialog() {
        if (loadingDialog != null)
            loadingDialog!!.dismiss()
    }

    protected fun generateMessageDialog(
        title: String, message: String, buttonText: String, useOfButtonRequiered: Boolean,
        onclickListener: DialogInterface.OnClickListener?,
        onDismissListener: DialogInterface.OnDismissListener?
    ) {
        val messageDialogBuilder = AlertDialog.Builder(this@BaseActivity)

        messageDialogBuilder.setTitle(title)
        messageDialogBuilder.setMessage(message)
        messageDialogBuilder.setCancelable(!useOfButtonRequiered)

        messageDialogBuilder.setPositiveButton(buttonText, onclickListener)
        messageDialogBuilder.setOnDismissListener(onDismissListener)

        messageDialogBuilder.create().show()
    }

    fun hideSystemUI() {
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE)
    }
}
