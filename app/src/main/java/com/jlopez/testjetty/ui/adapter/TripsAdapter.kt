package com.jlopez.testjetty.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.jlopez.testjetty.R
import com.jlopez.testjetty.common.Constant
import com.jlopez.testjetty.network.response.DataGeneralResponse
import com.jlopez.testjetty.ui.TripsStopsActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_trips.view.*


class TripsAdapter(
    val context: Context,
    private val items: List<DataGeneralResponse>) : RecyclerView.Adapter<TripsAdapter.TripViewHolder>() {

    private var lastPosition = 0
    private var dateShowing = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_trips, parent, false)
        return TripViewHolder(view)
    }

    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        if (position > lastPosition) {
            holder.cvContentCard?.animation = AnimationUtils.loadAnimation(context,
                R.anim.animation_fall_rigth
            );
            lastPosition = position
        }

        if(isShowDate(position)) {
            holder.tvHeader?.visibility = View.VISIBLE
            holder.tvHeader?.text = items[position].createTime
        } else
            holder.tvHeader?.visibility = View.GONE


        holder.tvBrand?.text = items[position].vehicle.brand
        holder.tvModel?.text = items[position].vehicle.model
        holder.tvPlate?.text = items[position].vehicle.plate

        holder.tvName?.text = items[position].name
        holder.tvHour?.text = items[position].time
        holder.tvFirstStop?.text = items[position].tripStops.first().name
        holder.tvLastStop?.text = items[position].tripStops.last().name

        Picasso.get()
            .load(items[position].vehicle.photo)
            .tag(context)
            .into(holder.cvPhoto)

        holder.bnSeeStops?.setOnClickListener {
            val intent = Intent(context, TripsStopsActivity::class.java)
            intent.putExtra(Constant.Extra.ITEM, items[position])
            context.startActivity(intent)
        }
    }

    override fun getItemCount() = items.size

    fun isShowDate(position: Int) : Boolean {
        var isShow: Boolean
        if(dateShowing != items[position].date) {
            dateShowing = items[position].date
            isShow = true
        } else {
            isShow = false
        }
        return isShow
    }

    class TripViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvHeader: TextView? = itemView.textHeader
        val cvContentCard: CardView? = itemView.cardContent

        val tvBrand: TextView? = itemView.brand
        val tvModel: TextView? = itemView.model
        val tvPlate: TextView? = itemView.plate
        val cvPhoto: ImageView? = itemView.photoVehicle

        val tvName: TextView? = itemView.name_trips
        val tvHour: TextView? = itemView.hour_trips
        val tvFirstStop: TextView? = itemView.first_stop_trips
        val tvLastStop: TextView? = itemView.last_stop_trips
        val bnSeeStops: Button? = itemView.see_stops
    }
}