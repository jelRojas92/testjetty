package com.jlopez.testjetty.ui

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.jlopez.testjetty.R
import com.jlopez.testjetty.ui.adapter.TripStopAdapter
import com.jlopez.testjetty.common.Constant
import com.jlopez.testjetty.network.response.DataGeneralResponse
import kotlinx.android.synthetic.main.activity_trips_stops.*


class TripsStopsActivity : BaseActivity() {
    lateinit var dataGeneralResponse: DataGeneralResponse
    private var tripStopAdapter: TripStopAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trips_stops)
        val bundle = intent.extras
        if (bundle != null) {
            dataGeneralResponse = bundle.getSerializable(Constant.Extra.ITEM) as DataGeneralResponse
            title = dataGeneralResponse.name
            tripStopAdapter =
                TripStopAdapter(this@TripsStopsActivity, dataGeneralResponse.tripStops)
            dataStops.layoutManager = LinearLayoutManager(this@TripsStopsActivity)
            dataStops.adapter = tripStopAdapter
        } else {
            Toast.makeText(this@TripsStopsActivity, getString(R.string.error_load_stops), Toast.LENGTH_SHORT).show()
            finish()
        }
    }
}
