package com.jlopez.testjetty.network.response

open class ErrorResponse() {
    var statusCode: Int  = 0
    var throwable: Throwable? = null
}