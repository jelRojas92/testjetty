package com.jlopez.testjetty.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.*
import java.util.concurrent.TimeUnit


object RetrofitHelper {
    fun getAPIService(): APIService {
        return RetrofitClient.getClient()!!.create(APIService::class.java)
    }
}