package com.jlopez.testjetty.network

import com.jlopez.testjetty.common.Constant
import com.jlopez.testjetty.network.request.LoginRequest
import com.jlopez.testjetty.network.response.DataGeneralResponse
import com.jlopez.testjetty.network.response.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface APIService {
    @POST(Constant.WebServices.POST_LOGIN)
    fun loginUser(@Header(Constant.Headers.CONTENT_TYPE) contentType: String,
                  @Body request: LoginRequest): Call<LoginResponse>

    @GET(Constant.WebServices.GET_TRIPS)
    fun getTrips(@Header(Constant.Headers.CONTENT_TYPE) contentType: String,
                 @Header(Constant.Headers.AUTHORIZATION) auth: String): Call<List<DataGeneralResponse>>
}