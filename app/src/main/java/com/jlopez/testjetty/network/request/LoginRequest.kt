package com.jlopez.testjetty.network.request

import com.google.gson.annotations.SerializedName
import com.jlopez.testjetty.common.Constant

data class LoginRequest (
    @SerializedName(Constant.Params.DRIVER)
    var driver: Driver
)