package com.jlopez.testjetty.network.response

import com.google.gson.annotations.SerializedName
import com.jlopez.testjetty.common.Constant
import java.io.Serializable

data class DriverResponse (
    @SerializedName(Constant.Params.ID)
    var id: Int,
    @SerializedName(Constant.Params.EMAIL)
    var email: String,
    @SerializedName(Constant.Params.NAME)
    var name: String,
    @SerializedName(Constant.Params.PHOTO)
    var photo: String,
    @SerializedName(Constant.Params.MOBILE)
    var mobile: String,
    @SerializedName(Constant.Params.LAST_NAME)
    var lastName: String
)  : Serializable