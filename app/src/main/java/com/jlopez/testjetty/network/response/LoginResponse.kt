package com.jlopez.testjetty.network.response

import com.google.gson.annotations.SerializedName
import com.jlopez.testjetty.common.Constant

data class LoginResponse (
    @SerializedName(Constant.Params.ID)
    var id: Int,
    @SerializedName(Constant.Params.EMAIL)
    var email: String,
    @SerializedName(Constant.Params.AUTH_TOKEN)
    var authToken: String,
    @SerializedName(Constant.Params.NAME)
    var name: String,
    @SerializedName(Constant.Params.LAST_NAME)
    var lastName: String,
    @SerializedName(Constant.Params.MOBILE)
    var mobile: String,
    @SerializedName(Constant.Params.FIREBASE_TOKEN)
    var firebaseToken: String
)