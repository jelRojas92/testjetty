package com.jlopez.testjetty.network.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.jlopez.testjetty.common.Constant
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

data class DataGeneralResponse (
    @SerializedName(Constant.Params.ID)
    var id: Int,
    @SerializedName(Constant.Params.NAME)
    var name: String,
    @SerializedName(Constant.Params.PRICE)
    var price: String,
    @SerializedName(Constant.Params.DATE)
    var date: String,
    @SerializedName(Constant.Params.TIME)
    var time: String,
    @SerializedName(Constant.Params.DRIVER)
    var drive: DriverResponse,
    @SerializedName(Constant.Params.VEHICLE)
    val vehicle: VehicleResponse,
    @SerializedName(Constant.Params.TRIP_STOPS)
    val tripStops: List<TripStopsResponse>
) : Serializable {
    val createTime: String?
        get() {
            try {
                val dateFormat = SimpleDateFormat(Constant.Format.DATE_FORMAT_CONVERTER, Locale.getDefault())
                return dateFormat.format(dateTime?.time)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

    val dateTime: Date?
        get() {
            try {
                val dateParser = SimpleDateFormat(Constant.Format.DATE_FORMAT, Locale.getDefault())
                return dateParser.parse(date)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }
}