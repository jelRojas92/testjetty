package com.jlopez.testjetty.network.response

import com.google.gson.annotations.SerializedName
import com.jlopez.testjetty.common.Constant
import java.io.Serializable

data class VehicleResponse (
    @SerializedName(Constant.Params.ID)
    var id: Int,
    @SerializedName(Constant.Params.BRAND)
    var brand: String,
    @SerializedName(Constant.Params.MODEL)
    var model: String,
    @SerializedName(Constant.Params.OWNER)
    var owner: String,
    @SerializedName(Constant.Params.PLATE)
    var plate: String,
    @SerializedName(Constant.Params.SEATS_NUMBER)
    var seatsNumber: Int,
    @SerializedName(Constant.Params.YEAR)
    var year: String,
    @SerializedName(Constant.Params.PHOTO)
    var photo: String
) : Serializable