package com.jlopez.testjetty.network.request

import com.google.gson.annotations.SerializedName
import com.jlopez.testjetty.common.Constant

data class Driver (
    @SerializedName(Constant.Params.EMAIL)
    var email: String,
    @SerializedName(Constant.Params.PASSWORD)
    var password: String
)