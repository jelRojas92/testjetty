package com.jlopez.testjetty.network.response

import com.google.gson.annotations.SerializedName
import com.jlopez.testjetty.common.Constant
import java.io.Serializable

data class TripStopsResponse (
    @SerializedName(Constant.Params.ID)
    var id: Int,
    @SerializedName(Constant.Params.NAME)
    var name: String,
    @SerializedName(Constant.Params.ADDRESS)
    var address: String,
    @SerializedName(Constant.Params.LATITUDE)
    var latitude: String,
    @SerializedName(Constant.Params.LONGITUDE)
    var longitude: String,
    @SerializedName(Constant.Params.DEPARTURE_TIME)
    var departureTime: String,
    @SerializedName(Constant.Params.BOARDING)
    var boarding: Boolean
) : Serializable