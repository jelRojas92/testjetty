package com.jlopez.testjetty.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.jlopez.testjetty.repository.LoginRepository
import androidx.lifecycle.LiveData
import com.jlopez.testjetty.network.response.LoginResponseOnComplete


class LoginViewModel : AndroidViewModel {
    var loginRepository: LoginRepository = LoginRepository()
    constructor(application: Application) : super(application)

    fun loginUser(email: String, password: String): LiveData<LoginResponseOnComplete> {
        return loginRepository.loginUser(email, password)
    }
}