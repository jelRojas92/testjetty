package com.jlopez.testjetty.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.jlopez.testjetty.network.response.LoginResponseOnComplete
import com.jlopez.testjetty.network.response.TripsResponseOnComplete
import com.jlopez.testjetty.repository.LoginRepository
import com.jlopez.testjetty.repository.TripsRepository

class TripsViewModel : AndroidViewModel {
    var tripsViewModel: TripsRepository = TripsRepository()
    constructor(application: Application) : super(application)

    fun TripsViewModel(application: Application) {
        tripsViewModel =  TripsRepository()
    }

    fun loginUser(auth: String): LiveData<TripsResponseOnComplete> {
        return tripsViewModel.getTrips(auth)
    }
}