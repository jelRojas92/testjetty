package com.jlopez.testjetty.utils

import android.content.Context
import android.preference.PreferenceManager
import com.jlopez.testjetty.common.Constant

object PreferencesHelper {
    fun setAuth(context: Context, token: String ) {
        val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
        editor.putString(Constant.Params.AUTH_TOKEN, token)
        editor.apply()
    }

    fun getAutn(context: Context): String? {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(
            Constant.Params.AUTH_TOKEN,
            Constant.Preferences.VALUE
        )
    }

    fun setEmail(context: Context, email: String ) {
        val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
        editor.putString(Constant.Params.EMAIL, email)
        editor.apply()
    }

    fun getEmail(context: Context): String? {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(
            Constant.Params.EMAIL,
            Constant.Preferences.VALUE
        )
    }
}