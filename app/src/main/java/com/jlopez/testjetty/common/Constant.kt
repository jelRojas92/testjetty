package com.jlopez.testjetty.common

object Constant {
    object WebServices {
        const val POST_LOGIN = "session"
        const val GET_TRIPS = "trips"
    }
    object Headers {
        const val CONTENT_TYPE = "Content-Type"
        const val CONTENT_TYPE_VALUE = "application/json"
        const val AUTHORIZATION = "Authorization"
        const val AUTHORIZATION_VALUE = "Token %s,email=%s"
    }
    object Extra {
        const val ITEM = "ITEM"
    }
    object Preferences {
        const val TOKEN = "token"
        const val EMAIL = "email"
        const val VALUE = ""
    }
    object Format {
        const val DATE_FORMAT = "yyyy-MM-dd"
        const val DATE_FORMAT_CONVERTER = "dd MMMM"
    }
    object Params {
        const val PASSWORD = "password"

        const val AUTH_TOKEN = "auth_token"
        const val FIREBASE_TOKEN = "firebase_token"

        const val DATA = "data"
        const val ID = "id"
        const val NAME = "name"
        const val PRICE = "price"
        const val DATE = "date"
        const val TIME = "time"

        const val DRIVER = "driver"
        const val EMAIL = "email"
        const val PHOTO = "photo"
        const val MOBILE = "mobile"
        const val LAST_NAME = "last_name"

        const val VEHICLE = "vehicle"
        const val BRAND = "brand"
        const val MODEL = "model"
        const val OWNER = "owner"
        const val PLATE = "plate"
        const val SEATS_NUMBER = "seats_number"
        const val YEAR = "year"

        const val TRIP_STOPS = "trip_stops"
        const val ADDRESS = "address"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
        const val DEPARTURE_TIME = "departure_time"
        const val BOARDING = "boarding"
    }
}